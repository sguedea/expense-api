package com.expenses.backend.services;

import java.util.List;

import com.expenses.backend.models.Expense;
import com.expenses.backend.repositories.ExpenseRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExpenseServiceImpl implements ExpenseService {

    @Autowired
    ExpenseRepository expenseRepo;

    @Override
    public List<Expense> findAll() {
        return expenseRepo.findAll();
    }

    @Override
    public Expense save(Expense expense) {
        expenseRepo.save(expense);
        return expense;
    }

    @Override
    public Expense findById(long id) {

        if (expenseRepo.findById(id).isPresent()) {
            return expenseRepo.findById(id).get();
        }
        return null;
    }

    @Override
    public void delete(long id) {
        findById(id);
        Expense e = findById(id);
        expenseRepo.delete(e);
    }


    
}

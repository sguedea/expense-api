package com.expenses.backend.services;

import java.util.List;

import com.expenses.backend.models.Expense;

public interface ExpenseService {
    List<Expense> findAll();

    // saving a new expense
    Expense save(Expense expense);

    // update
    Expense findById(long id);

    // delete
    void delete(long id);
}

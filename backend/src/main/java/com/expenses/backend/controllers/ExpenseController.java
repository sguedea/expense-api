package com.expenses.backend.controllers;

import java.util.List;

import com.expenses.backend.models.Expense;
import com.expenses.backend.services.ExpenseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/")
public class ExpenseController {

    @Autowired
    ExpenseService expenseService;

    @GetMapping("/expenses")
    public ResponseEntity<List<Expense>> get() {
        // List<Expense> expenses = expenseService.findAll();
        // return new ResponseEntity<List<Expense>>(expenses, HttpStatus.OK);
        return ResponseEntity.ok(expenseService.findAll());
    }

    @PostMapping("/expenses")
    public ResponseEntity<Expense> save(@RequestBody Expense newExpense) {
        Expense expense = expenseService.save(newExpense);
        return new ResponseEntity<Expense> (expense, HttpStatus.OK);
        // return ResponseEntity.ok(expenseService.save(newExpense));
    }

    @GetMapping("/expenses/{id}")
    public ResponseEntity<Expense> get(@PathVariable("id") long id) {
        Expense e = expenseService.findById(id);
        return new ResponseEntity<Expense>(e, HttpStatus.OK);
    
        // return ResponseEntity.ok(expenseService.findById(id));
    }

    @DeleteMapping("/expenses/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") long id) {
        expenseService.delete(id);
        return new ResponseEntity<String>("Expense deleted", HttpStatus.OK);
    
        // return ResponseEntity.ok(expenseService.findById(id));
    }




}
